"use strict"

createButton()

document.addEventListener("click", () => {
    if (isAnyTextSelected()) {
        showButton()
    } else {
        hideButton()
    }
})

function isAnyTextSelected() {
    return window.getSelection().toString() !== ""
}

function getSelectedText() {
    return window.getSelection().toString()
}

function clearSelectedText() {
    window.getSelection().empty()
}

function createButton() {
    const button = document.createElement("button")
    button.id = "__SENTENCE_CLIPPER__"
    button.style = `
        display: none;
        z-index: 2147483647;
        position: fixed;
        width: 100%; height: 10%;
        bottom: 0; right: 0;
        background-color: lightgray !important;
        font-size: 25px !important;
        font-family: sans-serif !important; 
    `
    button.onclick = () => {
        appendClipping(getSelectedText())
        clearSelectedText()
        hideButton()
    }
    button.appendChild(document.createTextNode("✂️"))
    document.body.appendChild(button)
}

function showButton() {
    document.querySelector("#__SENTENCE_CLIPPER__").style.display = null
}

function hideButton() {
    document.querySelector("#__SENTENCE_CLIPPER__").style.display = "none"
}

function appendClipping(text) {
    browser.storage.local.get("clippings").then(({ clippings }) => {
        const existing = clippings || []
        browser.storage.local.set({
            clippings: [text, ...existing]
        })
    })
}
