"use strict"

let _clippings
loadClippings()

document.querySelector("#copy-all").onclick = copyAll
document.querySelector("#clear-all").onclick = openClearAllConfirmation
document.querySelector("#clear-all-yes").onclick = clearAll
document.querySelector("#clear-all-no").onclick = closeClearAllConfirmation

browser.storage.onChanged.addListener((changes, area) => {
    if (area !== "local") {
        return
    }

    const hasChanged = Object.keys(changes).includes("clippings")
    if (!hasChanged) {
        return;
    }
    
    loadClippings()
})

function loadClippings() {
    browser.storage.local.get("clippings").then(({ clippings }) => {
        _clippings = clippings
        const text = document.createTextNode(prettyPrintClippings(clippings))
        const container = document.querySelector("#clippings")
        if (container.firstChild) {
            container.replaceChild(text, container.firstChild)
        } else {
            container.appendChild(text)
        }
    })
}

function prettyPrintClippings(clippings) {
    return clippings.join("\n──────────\n")
}

function copyToClipboard(text) {
    navigator.clipboard.writeText(text)
        .catch(console.error)
}

function copyAll() {
    copyToClipboard(prettyPrintClippings(_clippings))
}

function openClearAllConfirmation() {
    document.body.classList.add("stop-scrolling")
    document.querySelector("#clear-all-confirm").style.display = "block"
}

function closeClearAllConfirmation() {
    document.body.classList.remove("stop-scrolling")
    document.querySelector("#clear-all-confirm").style.display = "none"
}

function clearAll() {
    browser.storage.local.set({
        clippings: []
    })
    document.body.classList.remove("stop-scrolling")
    document.querySelector("#clear-all-confirm").style.display = "none"
}
